# mygranny-tv--to-preserve-and-protect

Half the world's people are closer to death than the opposite half.  By construction the elder half has experienced more events and been exposed to more knowledge. 